export interface Pokemon {
    id: number;
    name: string;
    sprites: string;
    types: string;
    stats: {
      attack: number;
      hp: number;
      speed: number;
      defense: number;
      special_attack: number;
      special_defense: number;
    };
    __v?: number;
  }