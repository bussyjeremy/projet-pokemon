import { Component, OnInit } from "@angular/core";
import { PokemonService } from "../../services/pokemon.service";
import { UserService } from "../../services/user.service";
import { Pokemon } from "../../interfaces/pokemon";
import { MenuComponent } from "../menu/menu.component";

@Component({
  selector: "app-page-boutique",
  templateUrl: "./page-boutique.component.html",
  styleUrls: ["./page-boutique.component.scss"]
})
export class PageBoutiqueComponent implements OnInit {
  pokemonIdMin: number = 1;
  pokemonIdMax: number = 200;
  listePokemon: Array<Pokemon> = [];
  listeTempPokemon: Array<Pokemon> = [];
  loading: boolean = false;
  prixBooster: number = 10;
  remboursementCarte: number = 1;
  

  constructor(
    private pokemon: PokemonService,
    private user: UserService,
    private menu: MenuComponent
  ) {}

  ngOnInit() {
    this.menu.loadMenuCoins();
  }

  entierAleatoire(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

   async showPokemon(nombre: number) {
    if (this.user.getUserCoins() >= this.prixBooster) {
      this.user.setUserCoins(-this.prixBooster);
      this.loading = true;
      for (let i = 0; i < nombre; i++) {
        let id = this.entierAleatoire(this.pokemonIdMin, this.pokemonIdMax);
        this.listeTempPokemon[i] = await this.pokemon.chargerPokemon(id);
      }
      this.listePokemon = this.listeTempPokemon;
      this.loading = false;
     }
     this.menu.loadMenuCoins();
  }

  delete(id: number) {
    console.log("carte supprimée");
    this.listePokemon.splice(id, 1);
    this.user.setUserCoins(this.remboursementCarte);
    this.menu.loadMenuCoins();
  }

  addDeck(id: number) {
    console.log("carte ajoutée");
    console.log(this.listePokemon[id].id);
    
    let idPokemon = this.listePokemon[id].id;
    this.user.setUserDeck(idPokemon);
    this.listePokemon.splice(id, 1);
  }
}