import { Component, OnInit } from "@angular/core";
import { UserService } from "src/app/services/user.service";
import { UserCoinService } from "src/app/services/user-coin.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.scss"]
})
export class MenuComponent implements OnInit {
  userCoin: number;

  constructor(
    private user: UserService,
    private userCoins: UserCoinService,
    private router: Router
  ) {}

  delete() {
    this.user.deleteUser();
    this.router.navigate(["/"]);
  }
  
  async ngOnInit() {
    this.userCoin = this.user.getUserCoins();

    this.userCoins.currentCoin = this.userCoin;
  }
  loadMenuCoins(){
    this.userCoin = this.user.getUserCoins();

    this.userCoins.currentCoin = this.userCoin;
  }
}