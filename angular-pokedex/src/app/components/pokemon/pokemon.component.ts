import { Component, OnInit, Input } from "@angular/core";
import { Pokemon } from "../../interfaces/pokemon";
import { PokemonService } from "../../services/pokemon.service";

@Component({
  selector: "app-pokemon",
  templateUrl: "./pokemon.component.html",
  styleUrls: ["./pokemon.component.scss"]
})
export class PokemonComponent implements OnInit {
  @Input() data: Pokemon;
  @Input() id: number;

  constructor() {}

  monPokemon: Pokemon;
  type: string;
  sprite: string;
  
  ngOnInit() {
    this.monPokemon = this.data;
    console.log(this.monPokemon);
    if (this.monPokemon.types.length > 1) { this.type = this.monPokemon.types[0]['type']['name'] + "/" + this.monPokemon.types[1]['type']['name']; }
    else { this.type = this.monPokemon.types[0]['type']['name']; }
    this.monPokemon.types = this.type;
    
    this.monPokemon.sprites = this.monPokemon.sprites['back_default'];
    this.monPokemon.stats.hp = this.monPokemon.stats[0]['base_stat'];
    this.monPokemon.stats.attack = this.monPokemon.stats[1]['base_stat'];
    this.monPokemon.stats.defense = this.monPokemon.stats[2]['base_stat'];
    this.monPokemon.stats.special_attack = this.monPokemon.stats[3]['base_stat'];
    this.monPokemon.stats.special_defense = this.monPokemon.stats[4]['base_stat'];
    this.monPokemon.stats.speed = this.monPokemon.stats[5]['base_stat'];
  }

  
}