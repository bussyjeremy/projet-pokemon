import { Component, OnInit, Input } from "@angular/core";
import { UserService } from "src/app/services/user.service";
import { Router } from "@angular/router";
import { MenuComponent } from 'src/app/components/menu/menu.component';

@Component({
  selector: 'app-formulaire-login',
  templateUrl: './formulaire-login.component.html',
  styleUrls: ['./formulaire-login.component.scss']
})
export class FormulaireLoginComponent implements OnInit {
  @Input() pseudo: string;

  constructor(private user: UserService, private router: Router, private menu: MenuComponent) { }

  ngOnInit() {
    this.menu.loadMenuCoins();
  }
  
  getUser() {
    this.user.setUserName(this.pseudo);    
    this.router.navigate(["deck"]);
  }
}