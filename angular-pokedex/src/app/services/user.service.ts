import { Injectable, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { User } from "../interfaces/user";
import { Pokemon } from "../interfaces/pokemon";

@Injectable()
export class UserService implements OnInit {
  constructor(private http: HttpClient) {}
  user: User = {
    name: '',
    coins: 100,
    deck: []
  };

  ngOnInit() {}

  getTokenUser(name: string): Promise<string> {
    return this.http
      .post<string>("/interface/user/login", {
        name: name
      })
      .toPromise();
  }

  getUser() {
    return this.user;
  }
  deleteUser(){
    this.user.name = '';
    this.user.coins = 100;
    this.user.deck = [];
  }

  getUserName() {
    return this.user.name;
  }
  setUserName(new_name: string) {
    this.user.name = new_name;
  }

  getUserCoins(){
    return this.user.coins;
  }
  setUserCoins(cost: number){
    this.user.coins += cost;
  }

  getUserDeck(){
    return this.user.deck;
  }
  setUserDeck(pokemon_ID: number){
    this.user.deck.push(pokemon_ID);
  }
}
