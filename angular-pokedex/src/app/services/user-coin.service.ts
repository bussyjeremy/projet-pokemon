import { Injectable } from "@angular/core";
import { User } from "src/app/interfaces/user";
import { UserService } from "./user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class UserCoinService {
  currentUser: User;
  currentCoin: number;

  constructor(private userService: UserService, private http: HttpClient) {}

  load() {
    this.currentUser = this.userService.getUser();
    this.currentCoin = this.currentUser.coins;
    console.log(this.currentCoin);
    return this.currentCoin;
  }
}