import { TestBed } from "@angular/core/testing";

import { UserCoinService } from "./user-coin.service";

describe("UserCoinsService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: UserCoinService = TestBed.get(UserCoinService);
    expect(service).toBeTruthy();
  });
});