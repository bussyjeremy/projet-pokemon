import { Pipe, PipeTransform } from "@angular/core";
import { Pokemon } from "../interfaces/pokemon";

@Pipe({
  name: "filter"
})
export class FilterPipe implements PipeTransform {
  transform(listPokemon: Array<Pokemon>, minAttack: number): Array<Pokemon> {
    if (!minAttack) {
      return listPokemon;
    }
    return listPokemon.filter(card => {
      if (card.stats.attack >= minAttack) {
        return card;
      }
      else{
        return '';
      }
    });
  }
}