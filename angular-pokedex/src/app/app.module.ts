import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormulaireLoginComponent } from './components/formulaire-login/formulaire-login.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PokemonService } from "./services/pokemon.service";
import { UserService } from "./services/user.service";
import { MenuComponent } from './components/menu/menu.component';
import { PageErrorComponent } from './components/page-error/page-error.component';
import { PageBoutiqueComponent } from './components/page-boutique/page-boutique.component';
import { PageDeckComponent } from './components/page-deck/page-deck.component';
import { PageLoginComponent } from './components/page-login/page-login.component';
import { PokemonComponent } from './components/pokemon/pokemon.component';
import { FilterPipe } from './pipe/filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    FormulaireLoginComponent,
    MenuComponent,
    PageErrorComponent,
    PageBoutiqueComponent,
    PageDeckComponent,
    PageLoginComponent,
    PokemonComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [PokemonService, UserService, MenuComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
